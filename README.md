# API Endpoint User Management and parsing data processing

Project ini adalah sebagai pendukung project [data processing](https://gitlab.com/baysptr/data-processing-for-socials-media-monitoring) guna untuk management users with role dan bagaimana parsing data hasil processing data pada project sebelumnnya.
## Fiture
  - [x] User Management
  - [x] Role Management
  - [x] Account Crawler Management
  - [x] Monitoring target Management
  - [ ] JWT Login
  - [ ] Access data processing 

## Prerequirement
- create collection in database mongo on names
- account_crawler
- monitor
- user_management

## How to Install (Using Python3.7)
```sh
$ pip3 or pip install virtualenv
$ virtualenv venv
$ source venv/bin/active
$ pip install requirement.txt -r
$ python app.py
```
